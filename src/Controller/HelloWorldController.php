<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class HelloWorldController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function sayHello(): Response
    {
        return $this->json(['message' => 'Symfony Hello World !']);
    }
}
