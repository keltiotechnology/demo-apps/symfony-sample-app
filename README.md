# Symfony sample app

Provide sample Symfony PHP application.

To use the application with Nginx, you may want to update Dockerfile to have ```php-fpm``` running instead of calling ```symfony serve```.


## Using Dockerfile

### Build Docker image

Pack symfony application in Docker and export default port 8000.

```bash
docker build -t  symfony:latest .
```

Have symfony application serve in different port

```bash
dockebuild -t  symfony --build-arg PORT=9000 .
```

Use different PHP version as base image

```bash
dockebuild -t  symfony --build-arg PHP_VERSION=<PHP_VERSIOn> .
```

### Run Docker image

```bash
docker container run -t --rm -p 8080:8000 symfony:latest
```

## References

Reference for more mature Dockerfile:
- https://github.com/dunglas/symfony-docker/blob/main/Dockerfile