ARG PHP_VERSION=8-fpm-buster
ARG PORT=8000

FROM php:${PHP_VERSION}
ARG PORT
# Install the PHP extensions Symfony depends on.
# Reference: https://www.twilio.com/blog/get-started-docker-symfony
RUN apt-get update \
    && apt-get install -y zlib1g-dev g++ git libicu-dev zip libzip-dev zip \
    && docker-php-ext-install intl opcache pdo pdo_mysql \
    && pecl install apcu \
    && docker-php-ext-enable apcu \
    && docker-php-ext-configure zip \
    && docker-php-ext-install zip
    # && dumb-init
RUN apt-get install -y dumb-init

WORKDIR /var/www
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN curl -sS https://get.symfony.com/cli/installer | bash
RUN mv /root/.symfony/bin/symfony /usr/local/bin/symfony

RUN composer self-update

# # Add user for symfony application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

# Copy existing application directory permissions
COPY --chown=www:www . /var/www

RUN composer install

WORKDIR /var/www

# # Change current user to www
USER www

# # Expose port and start php-fpm server
ENV PORT=${PORT}
EXPOSE ${PORT}

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["sh", "-c","symfony serve --port $PORT"]
# To use with Nginx
# CMD ["php-fpm"]